package avaturi

import (
	"fmt"
	"net/url"
)

func normalizeUri(uri *url.URL) {
	if uri.Path == "" {
		uri.Path = "/"
	}
}

func Avatar(uri string) (string, error) {
	u, err := url.Parse(uri)
	if err != nil {
		return "", err
	}
	normalizeUri(u)
	methods := []func(*url.URL) string{}
	if u.Scheme == "http" || u.Scheme == "https" {
		// find avatar for http uri
		methods = append(methods, MatchMicroformatHCardPhoto)
	} else if u.Scheme == "acct" {
		// find avatar for acct uri
		// TODO: implement
	}
	for _, f := range methods {
		if avatar := f(u); avatar != "" {
			return avatar, nil
		}
	}
	return "", fmt.Errorf("no avatar found for %s", u)
}
