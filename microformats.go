package avaturi

import (
	"log"
	"net/http"
	"net/url"

	"willnorris.com/go/microformats"
)

func parseURL(uri *url.URL) (*microformats.Data, error) {
	resp, err := http.Get(uri.String())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	data := microformats.Parse(resp.Body, uri)
	return data, nil
}

func deduplicateMicroformats(mf []*microformats.Microformat) []*microformats.Microformat {
	mp := make(map[*microformats.Microformat]bool)
	nmf := []*microformats.Microformat{}

	for _, v := range mf {
		if _, exists := mp[v]; !exists {
			mp[v] = true
			nmf = append(nmf, v)
		}
	}
	return nmf
}

func findPropertyChildren(mf *microformats.Microformat) []*microformats.Microformat {
	children := []*microformats.Microformat{}
	for _, v := range mf.Properties {
		for _, c := range v {
			switch i := c.(type) {
			case *microformats.Microformat:
				children = append(children, i)
			}
		}
	}
	return children
}

func findByType(mf []*microformats.Microformat, mfType string) []*microformats.Microformat {
	mformats := []*microformats.Microformat{}
	for _, v := range mf {
		for _, t := range v.Type {
			if t == mfType {
				mformats = append(mformats, v)
				break
			}
		}
		mformats = append(mformats, findByType(v.Children, mfType)...)
		mformats = append(mformats, findByType(findPropertyChildren(v), mfType)...)
	}
	return mformats
}

func findWithProperty(mf []*microformats.Microformat, property string) []*microformats.Microformat {
	microformats := []*microformats.Microformat{}
	for _, v := range mf {
		if _, ok := v.Properties[property]; ok {
			microformats = append(microformats, v)
		}
		microformats = append(microformats, findWithProperty(v.Children, property)...)
		microformats = append(microformats, findWithProperty(findPropertyChildren(v), property)...)
	}
	return microformats
}

func findByProperty(mf []*microformats.Microformat, property, value string) []*microformats.Microformat {
	microformats := []*microformats.Microformat{}
	for _, v := range mf {
		for _, p := range v.Properties[property] {
			if p == value {
				microformats = append(microformats, v)
				break
			}
		}
		microformats = append(microformats, findByProperty(v.Children, property, value)...)
		microformats = append(microformats, findByProperty(findPropertyChildren(v), property, value)...)
	}
	return microformats
}

func findMissingProperty(mf []*microformats.Microformat, property string) []*microformats.Microformat {
	mformats := []*microformats.Microformat{}
	for _, v := range mf {
		if _, ok := v.Properties[property]; !ok {
			mformats = append(mformats, v)
		}
	}
	return mformats
}

func MatchMicroformatHCardPhoto(uri *url.URL) string {
	data, err := parseURL(uri)
	if err != nil {
		return ""
	}
	hcards := findByType(data.Items, "h-card")
	if len(hcards) == 0 {
		log.Println("No h-card found")
		return ""
	}
	urls := findByProperty(hcards, "url", uri.String())
	if len(urls) == 0 {
		urls = findMissingProperty(hcards, "url")
		if len(urls) == 0 {
			urls = hcards
		}
	}
	photos := findWithProperty(urls, "photo")
	if len(photos) == 0 {
		photos = findWithProperty(urls, "logo")
		if len(photos) == 0 {
			log.Println("No h-card with photo or logo found")
			return ""
		}
	}

	switch p := photos[0].Properties["photo"][0].(type) {
	case string:
		return p
	case map[string]string:
		return p["value"]
	}
	log.Println("match didn't work")
	return ""
}
