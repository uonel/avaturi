package main

import (
	"log"
	"net/http"

	"codeberg.org/uonel/avaturi"
)

// Serve Avaturi via HTTP
func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		avatar, err := avaturi.Avatar(r.URL.Query().Get("uri"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotFound)
			log.Println(err)
			return
		}
		w.Header().Set("Location", avatar)
		w.WriteHeader(http.StatusFound)
	})
	http.ListenAndServe(":8087", nil)
}
